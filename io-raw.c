/* -*- mode: C; c-file-style: "linux" -*- */
/* GdkPixbuf library - PSD image loader
 *
 * Copyright (C) 2008 Jan Dudek
 *
 * Authors: Jan Dudek <jd@jandudek.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * TODO
 * - use http://library.gnome.org/devel/glib/unstable/glib-Byte-Order-Macros.html
 * - report errors from parse_brute_header
 * - other color modes (CMYK at least)
 * - i18n
 */

//#define _XOPEN_SOURCE

#define GDK_PIXBUF_ENABLE_BACKEND

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <gmodule.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gdk-pixbuf-io.h>
#include <glib/gstdio.h>
#include <math.h>
#include <libraw.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct
{

	GdkPixbufModulePreparedFunc prepared_func;
	GdkPixbufModuleUpdatedFunc updated_func;
	GdkPixbufModuleSizeFunc       size_func;
	gpointer user_data;
/*
	gint            width;         // width of image in pixels (1-30000)
	gint            height;        // height of image in pixels (1-30000)
	gint            channels;      // number of color channels (1-24)
	gint            depth;         // number of bits per channel (1/8/16)
*/
	GByteArray                 *data;
} RAWData;


static GdkPixbuf* write_pixbuf(libraw_processed_image_t *img)
{
	unsigned int i = 0;
    	if(!img) 
		return NULL;
    	if(img->type != LIBRAW_IMAGE_BITMAP) 
		return NULL;
    	if(img->colors != 3) 
		return NULL;
#define SWAP(a,b) { a ^= b; a ^= (b ^= a); }
    	if (img->bits == 16 && htons(0x55aa) != 0x55aa)
        for(i=0; i< img->data_size; i+=2)
        	SWAP(img->data[i],img->data[i+1]);
#undef SWAP
	GdkPixbuf* pix = gdk_pixbuf_new_from_data (img->data, GDK_COLORSPACE_RGB, FALSE, 8, img->width, img->height, img->width * 3, NULL, NULL);
	return pix;
}

static gpointer
gdk_pixbuf__raw_image_begin_load (GdkPixbufModuleSizeFunc size_func,
                                  GdkPixbufModulePreparedFunc prepared_func,
                                  GdkPixbufModuleUpdatedFunc updated_func,
                                  gpointer user_data,
                                  GError **error)
{
	RAWData* context = g_new0(RAWData, 1);
    	(void)error;

    	context->prepared_func = prepared_func;
    	context->updated_func = updated_func;
    	context->size_func = size_func;
    	context->user_data = user_data;

    	context->data = g_byte_array_new();

    	return (gpointer)context;
}

/* Shared library entry point for file loading */
static GdkPixbuf *
gdk_pixbuf__raw_image_load_raw_data (guchar* data, gint size, GError **error)
{
	GdkPixbuf *pixbuf;
	int ret = -1;
	libraw_data_t* raw_data = libraw_init(0);
	
    	raw_data->params.half_size = 0; /* dcraw -h */
    	raw_data->params.user_qual = 2; /* dcraw -h */

	if((ret = libraw_open_buffer(raw_data, data, size)) != LIBRAW_SUCCESS)
        {
               	return NULL;
        }
       	if((ret = libraw_unpack(raw_data)) != LIBRAW_SUCCESS)
        {
		return NULL; 
        }
       	if((ret = libraw_dcraw_process(raw_data)) != LIBRAW_SUCCESS)
	{
               	if(LIBRAW_FATAL_ERROR(ret))
			return NULL;
        }
       	libraw_processed_image_t *image = libraw_dcraw_make_mem_image(raw_data,&ret);
       	if(image)
        {
        	pixbuf  = write_pixbuf(image);
               	//free(image);
        }
       	else
	{
       		libraw_close(raw_data);
		return NULL;
	}
	libraw_close(raw_data);
	return pixbuf;
}

static gboolean
gdk_pixbuf__raw_image_stop_load (gpointer context_ptr, GError **error)
{
	RAWData *context = (RAWData*) context_ptr;
        GdkPixbuf *pixbuf;
        gboolean retval = TRUE;

	g_return_val_if_fail (context_ptr != NULL, TRUE);
       
       	pixbuf = gdk_pixbuf__raw_image_load_raw_data (context->data->data, context->data->len, error);
       	if (pixbuf != NULL) {
	       	if (context->prepared_func)
		       	(* context->prepared_func) (pixbuf, NULL, context->user_data);
	       	if (context->updated_func)
		       	(* context->updated_func) (pixbuf, 0, 0, gdk_pixbuf_get_width(pixbuf), gdk_pixbuf_get_height(pixbuf), context->user_data);
               	//g_object_unref (pixbuf);
               	retval = TRUE;
       	}

    	g_byte_array_free(context->data, TRUE);
    	g_free(context);
        return retval;
}

static gboolean
gdk_pixbuf__raw_image_load_increment (gpointer      context_ptr,
                                      const guchar *data,
                                      guint         size,
                                      GError      **error)
{
	RAWData *context = (RAWData *) context_ptr;
	(void)error;
    	g_byte_array_append (context->data, data, size);

	return TRUE;
}


#ifndef INCLUDE_raw
#define MODULE_ENTRY(function) G_MODULE_EXPORT void function
#else
#define MODULE_ENTRY(function) void _gdk_pixbuf__raw_ ## function
#endif

MODULE_ENTRY (fill_vtable) (GdkPixbufModule* module)
{
	module->begin_load = gdk_pixbuf__raw_image_begin_load;
	module->stop_load = gdk_pixbuf__raw_image_stop_load;
	module->load_increment = gdk_pixbuf__raw_image_load_increment;
}

MODULE_ENTRY (fill_info) (GdkPixbufFormat *info)
{
	static GdkPixbufModulePattern signature[] = {
		{ (gchar*)"II", NULL, 100 },
		{ (gchar*)"MM", NULL, 100 },
		{ (gchar*)"FUJIFILMCCD-RAW", NULL, 100 },
		{ (gchar*)"FOVb", NULL, 100 },
		{ (gchar*)"\r\nMRM", NULL, 100 },/*"\x00MRM" =>extension "mrw" */
		{ NULL, NULL, 0 }
	};
	static gchar * mime_types[] = {
		(gchar*)"image/x-sony-srf",
		(gchar*)"image/x-sony-sr2",
		(gchar*)"image/x-sony-arw",
		(gchar*)"image/x-canon-crw",
		(gchar*)"image/x-canon-cr2",
		(gchar*)"image/x-kodak-dcr",
		(gchar*)"image/x-adobe-dng",
		(gchar*)"image/x-kodak-k25",
		(gchar*)"image/x-kodak-kdc",
		(gchar*)"image/x-minolta-mrw",
		(gchar*)"image/x-nikon-nef",
		(gchar*)"image/x-olympus-orf",
		(gchar*)"image/x-panasonic-raw",
		(gchar*)"image/x-pentax-pef",
		(gchar*)"image/x-fuji-raf",
		(gchar*)"image/x-sigma-x3f",
		NULL
	};
	static gchar * extensions[] = {
		(gchar*)"arw", 
		(gchar*)"sr2",
		(gchar*)"srf",
		(gchar*)"cr2",
		(gchar*)"crw",
		(gchar*)"dcr",
		(gchar*)"dng",
		(gchar*)"k25",
		(gchar*)"kdc",
		(gchar*)"mrw",
		(gchar*)"nef",
		(gchar*)"orf",
		(gchar*)"raw",
		(gchar*)"pef",
		(gchar*)"raf",
		(gchar*)"x3f",
		NULL
	};

	info->name = (gchar*)"raw";
	info->signature = signature;
	//info->description = N_("Adobe Photoshop format");
	info->description = (gchar*)"Raw digital camera image";
	info->mime_types = mime_types;
	info->extensions = extensions;
	info->flags = GDK_PIXBUF_FORMAT_THREADSAFE;
	info->flags = 0;
	info->license = (gchar*)"LGPL";
}

